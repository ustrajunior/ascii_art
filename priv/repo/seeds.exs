# Script for populating the database. You can run it as:
#
#     mix run priv/repo/seeds.exs
#
# Inside the script, you can read and write to any of your
# repositories directly:
#
#     AsciiArt.Repo.insert!(%AsciiArt.SomeSchema{})
#
# We recommend using the bang functions (`insert!`, `update!`
# and so on) as they will fail if something goes wrong.

alias AsciiArt.Drawing

{:ok, canva} = Drawing.create_canva()

Drawing.draw_rectangle(canva, %{
  point_x: 3,
  point_y: 2,
  width: 5,
  height: 3,
  outline: "@",
  fill: "X"
})

Drawing.draw_rectangle(canva, %{
  point_x: 10,
  point_y: 3,
  width: 14,
  height: 6,
  outline: "X",
  fill: "O"
})

{:ok, canva} = Drawing.create_canva()

{:ok, _} =
  Drawing.draw_rectangle(canva, %{
    point_x: 14,
    point_y: 0,
    width: 7,
    height: 6,
    outline: nil,
    fill: "."
  })

{:ok, _} =
  Drawing.draw_rectangle(canva, %{
    point_x: 0,
    point_y: 3,
    width: 8,
    height: 4,
    outline: "O",
    fill: nil
  })

{:ok, _} =
  Drawing.draw_rectangle(canva, %{
    point_x: 5,
    point_y: 5,
    width: 5,
    height: 3,
    outline: "X",
    fill: "X"
  })
