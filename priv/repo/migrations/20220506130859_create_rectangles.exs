defmodule AsciiArt.Repo.Migrations.CreateRectangles do
  use Ecto.Migration

  def change do
    create table(:rectangles, primary_key: false) do
      add(:id, :binary_id, primary_key: true)
      add(:point_x, :integer)
      add(:point_y, :integer)
      add(:width, :integer)
      add(:height, :integer)
      add(:fill, :string)
      add(:outline, :string)

      add(:canva_id, references("canvas"))

      timestamps()
    end
  end
end
