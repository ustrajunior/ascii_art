defmodule AsciiArtWeb.Drawing.CanvasControllerTest do
  use AsciiArtWeb.ConnCase
  import AsciiArt.DrawingFixtures

  describe "show/2" do
    test "return a canvas without rectangles", %{conn: conn} do
      canva = canva_fixture()

      conn = get(conn, Routes.canvas_path(conn, :show, Ecto.UUID.cast!(canva.id)))

      assert response = json_response(conn, 200)
      assert %{"height" => 500, "rectangles" => [], "width" => 500} = response
    end

    test "return a canvas without rectangles with custom sizes", %{conn: conn} do
      canva = canva_fixture(%{width: 200, height: 200})

      conn = get(conn, Routes.canvas_path(conn, :show, Ecto.UUID.cast!(canva.id)))

      assert response = json_response(conn, 200)
      assert %{"height" => 200, "rectangles" => [], "width" => 200} = response
    end

    test "return 404 if no canvas found", %{conn: conn} do
      conn = get(conn, Routes.canvas_path(conn, :show, Ecto.UUID.generate()))

      assert json_response(conn, 404)
    end

    test "return 404 if the id is not a valid uuid", %{conn: conn} do
      conn = get(conn, Routes.canvas_path(conn, :show, "not-valid"))

      assert json_response(conn, 404)
    end

    test "return a canvas with rectangles", %{conn: conn} do
      canva = canva_fixture()

      rectangle_fixture(canva, %{point_x: 3, point_y: 2})

      conn = get(conn, Routes.canvas_path(conn, :show, Ecto.UUID.cast!(canva.id)))

      assert response = json_response(conn, 200)

      assert %{
               "height" => 500,
               "rectangles" => [
                 [
                   %{"text" => "@", "x" => 3, "y" => 2},
                   %{"text" => "@", "x" => 3, "y" => 3},
                   %{"text" => "@", "x" => 3, "y" => 4},
                   %{"text" => "@", "x" => 4, "y" => 2},
                   %{"text" => "X", "x" => 4, "y" => 3},
                   %{"text" => "@", "x" => 4, "y" => 4},
                   %{"text" => "@", "x" => 5, "y" => 2},
                   %{"text" => "X", "x" => 5, "y" => 3},
                   %{"text" => "@", "x" => 5, "y" => 4},
                   %{"text" => "@", "x" => 6, "y" => 2},
                   %{"text" => "X", "x" => 6, "y" => 3},
                   %{"text" => "@", "x" => 6, "y" => 4},
                   %{"text" => "@", "x" => 7, "y" => 2},
                   %{"text" => "@", "x" => 7, "y" => 3},
                   %{"text" => "@", "x" => 7, "y" => 4}
                 ]
               ],
               "width" => 500
             } = response
    end

    test "return a canvas with multiple rectangles", %{conn: conn} do
      canva = canva_fixture()
      rectangle_fixture(canva, %{point_x: 0, point_y: 0})
      rectangle_fixture(canva, %{point_x: 10, point_y: 10})

      conn = get(conn, Routes.canvas_path(conn, :show, Ecto.UUID.cast!(canva.id)))

      assert response = json_response(conn, 200)

      rec_1 = [
        %{"text" => "@", "x" => 0, "y" => 0},
        %{"text" => "@", "x" => 0, "y" => 1},
        %{"text" => "@", "x" => 0, "y" => 2},
        %{"text" => "@", "x" => 1, "y" => 0},
        %{"text" => "X", "x" => 1, "y" => 1},
        %{"text" => "@", "x" => 1, "y" => 2},
        %{"text" => "@", "x" => 2, "y" => 0},
        %{"text" => "X", "x" => 2, "y" => 1},
        %{"text" => "@", "x" => 2, "y" => 2},
        %{"text" => "@", "x" => 3, "y" => 0},
        %{"text" => "X", "x" => 3, "y" => 1},
        %{"text" => "@", "x" => 3, "y" => 2},
        %{"text" => "@", "x" => 4, "y" => 0},
        %{"text" => "@", "x" => 4, "y" => 1},
        %{"text" => "@", "x" => 4, "y" => 2}
      ]

      rec_2 = [
        %{"text" => "@", "x" => 10, "y" => 10},
        %{"text" => "@", "x" => 10, "y" => 11},
        %{"text" => "@", "x" => 10, "y" => 12},
        %{"text" => "@", "x" => 11, "y" => 10},
        %{"text" => "X", "x" => 11, "y" => 11},
        %{"text" => "@", "x" => 11, "y" => 12},
        %{"text" => "@", "x" => 12, "y" => 10},
        %{"text" => "X", "x" => 12, "y" => 11},
        %{"text" => "@", "x" => 12, "y" => 12},
        %{"text" => "@", "x" => 13, "y" => 10},
        %{"text" => "X", "x" => 13, "y" => 11},
        %{"text" => "@", "x" => 13, "y" => 12},
        %{"text" => "@", "x" => 14, "y" => 10},
        %{"text" => "@", "x" => 14, "y" => 11},
        %{"text" => "@", "x" => 14, "y" => 12}
      ]

      assert %{
               "height" => 500,
               "rectangles" => rectangles,
               "width" => 500
             } = response

      assert Enum.all?(rectangles, fn rec -> rec in [rec_1, rec_2] end)
    end
  end
end
