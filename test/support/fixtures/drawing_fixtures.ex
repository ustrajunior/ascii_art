defmodule AsciiArt.DrawingFixtures do
  @moduledoc """
  This module defines test helpers for creating
  entities via the `AsciiArt.Drawing` context.
  """

  @doc """
  Generate a canva.
  """
  def canva_fixture(attrs \\ %{}) do
    {:ok, canva} =
      attrs
      |> Enum.into(%{height: 500, width: 500})
      |> AsciiArt.Drawing.create_canva()

    canva
  end

  def rectangle_fixture(canva, attrs \\ %{}) do
    {:ok, rectangle} =
      attrs
      |> Enum.into(%{point_x: 3, point_y: 2, width: 5, height: 3, outline: "@", fill: "X"})
      |> then(&AsciiArt.Drawing.draw_rectangle(canva, &1))

    rectangle
  end
end
