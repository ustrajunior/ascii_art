defmodule AsciiArt.DrawingTest do
  use AsciiArt.DataCase, async: true
  import AsciiArt.DrawingFixtures

  alias AsciiArt.Drawing
  alias AsciiArt.Drawing.Canva

  describe "canvas" do
    test "get_canva/1 returns the canva with given id" do
      canva = canva_fixture()

      assert Drawing.get_canva(canva.id) == {:ok, canva}
    end

    test "get_canva/1 returns an error if canva does not exists" do
      assert {:error, :not_found} = Drawing.get_canva(Ecto.UUID.generate())
    end

    test "create_canva/1 with valid data creates a canva" do
      valid_attrs = %{height: 500, width: 500}

      assert {:ok, %Canva{} = canva} = Drawing.create_canva(valid_attrs)
      assert canva.height == 500
      assert canva.width == 500
    end

    test "create_canva/1 with nil data returns error changeset" do
      assert {:error, %Ecto.Changeset{}} = Drawing.create_canva(%{height: nil, width: nil})
    end

    test "create_canva/1 with negative data returns error changeset" do
      assert {:error, %Ecto.Changeset{}} = Drawing.create_canva(%{height: -20, width: 100})
      assert {:error, %Ecto.Changeset{}} = Drawing.create_canva(%{height: 20, width: -100})
    end

    test "create_canva/1 with zero returns error changeset" do
      assert {:error, %Ecto.Changeset{}} = Drawing.create_canva(%{height: 0, width: 100})
      assert {:error, %Ecto.Changeset{}} = Drawing.create_canva(%{height: 20, width: 0})
    end

    test "create_canva/1 with invalid values for dimension returns error changeset" do
      assert {:error, %Ecto.Changeset{}} = Drawing.create_canva(%{height: 1001, width: 100})
      assert {:error, %Ecto.Changeset{}} = Drawing.create_canva(%{height: 20, width: 1001})
    end
  end

  describe "rectangle/5" do
    test "draw to rectangles for the give coordinates" do
      canva = canva_fixture()

      draw(canva, %{point_x: 3, point_y: 2, width: 5, height: 3, outline: "@", fill: "X"})
      draw(canva, %{point_x: 10, point_y: 3, width: 14, height: 6, outline: "X", fill: "O"})

      {:ok, canva} = Drawing.get_canva_with_associations(canva.id)

      assert rectangles = canva.rectangles
      assert length(rectangles) == 2

      assert %{point_x: 3, point_y: 2, width: 5, height: 3, outline: "@", fill: "X"} =
               Enum.find(rectangles, &(&1.point_x == 3))

      assert %{point_x: 10, point_y: 3, width: 14, height: 6, outline: "X", fill: "O"} =
               Enum.find(rectangles, &(&1.point_x == 10))
    end

    test "draw three rectangles for the given coordinates" do
      canva = canva_fixture()

      draw(canva, %{point_x: 14, point_y: 0, width: 7, height: 6, outline: nil, fill: "."})
      draw(canva, %{point_x: 0, point_y: 3, width: 8, height: 4, outline: "O", fill: nil})
      draw(canva, %{point_x: 5, point_y: 5, width: 5, height: 3, outline: "X", fill: "X"})

      {:ok, canva} = Drawing.get_canva_with_associations(canva.id)

      assert rectangles = canva.rectangles
      assert length(rectangles) == 3

      assert %{point_x: 14, point_y: 0, width: 7, height: 6, outline: nil, fill: "."} =
               Enum.find(rectangles, &(&1.point_x == 14))

      assert %{point_x: 0, point_y: 3, width: 8, height: 4, outline: "O", fill: nil} =
               Enum.find(rectangles, &(&1.point_x == 0))

      assert %{point_x: 5, point_y: 5, width: 5, height: 3, outline: "X", fill: "X"} =
               Enum.find(rectangles, &(&1.point_x == 5))
    end

    test "draw_rectangle/2 with both fill and outline nil returns error changeset" do
      canva = canva_fixture()

      {:error, %Ecto.Changeset{}} =
        Drawing.draw_rectangle(canva, %{
          point_x: 14,
          point_y: 0,
          width: 7,
          height: 6,
          outline: nil,
          fill: nil
        })
    end

    test "draw_rectangle/2 with negative values for points returns error changeset" do
      canva = canva_fixture()

      {:error, %Ecto.Changeset{} = changeset} =
        Drawing.draw_rectangle(canva, %{
          point_x: -14,
          point_y: -12,
          width: 7,
          height: 6,
          outline: "X",
          fill: "."
        })

      [error_point_x] = errors_on(changeset).point_x
      [error_point_y] = errors_on(changeset).point_y

      assert "must be greater than or equal to 0" == error_point_x
      assert "must be greater than or equal to 0" == error_point_y
    end

    test "draw_rectangle/2 with negative values for dimension returns error changeset" do
      canva = canva_fixture()

      {:error, %Ecto.Changeset{} = changeset} =
        Drawing.draw_rectangle(canva, %{
          point_x: 1,
          point_y: 1,
          width: -7,
          height: -6,
          outline: "X",
          fill: "."
        })

      [error_width] = errors_on(changeset).width
      [error_height] = errors_on(changeset).height

      assert "must be greater than 0" == error_width
      assert "must be greater than 0" == error_height
    end

    test "draw_rectangle/2 with invalid values for dimension returns error changeset" do
      canva = canva_fixture()

      {:error, %Ecto.Changeset{} = changeset} =
        Drawing.draw_rectangle(canva, %{
          point_x: 1,
          point_y: 1,
          width: 1001,
          height: 0,
          outline: "X",
          fill: "."
        })

      [error_width] = errors_on(changeset).width
      [error_height] = errors_on(changeset).height

      assert "must be less than or equal to 1000" == error_width
      assert "must be greater than 0" == error_height
    end

    defp draw(canva, points) do
      rectangle_fixture(canva, points)
    end
  end
end
