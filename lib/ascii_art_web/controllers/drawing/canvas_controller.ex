defmodule AsciiArtWeb.Drawing.CanvasController do
  @moduledoc """
  The Controller for the Canvas endpoints.

  An important point to notice, as the database used is SQLite,
  and the id is a UUID, we need to convert the id before doing the queries.

  The only endpoint available at the moment is the `show`.
  The `FallBackController` will respond with a 404 error if the query could not find a Canvas.
  """

  use AsciiArtWeb, :controller

  action_fallback AsciiArtWeb.FallbackController

  alias AsciiArt.Drawing

  def index(conn, _params) do
    canvas = Drawing.list_canvas()
    render(conn, "index.json", %{canvas: canvas})
  end

  def show(conn, %{"id" => canva_id}) do
    with {:ok, id} <- Ecto.UUID.dump(canva_id),
         {:ok, canva} <- Drawing.get_canva_with_associations(id) do
      render(conn, "show.json", %{canva: canva})
    else
      :error -> {:error, :not_found}
      err -> err
    end
  end
end
