defmodule AsciiArtWeb.FallbackController do
  @moduledoc false

  use Phoenix.Controller

  def call(conn, {:error, :not_found}) do
    conn
    |> put_status(:not_found)
    |> json(%{errors: %{status: "not_found"}})
  end
end
