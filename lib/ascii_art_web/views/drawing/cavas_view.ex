defmodule AsciiArtWeb.Drawing.CanvasView do
  use AsciiArtWeb, :view

  def render("index.json", %{canvas: canvas}) do
    render_many(canvas, __MODULE__, "canva.json", as: :canva)
  end

  def render("canva.json", %{canva: canva}) do
    %{
      id: Ecto.UUID.load!(canva.id),
      width: canva.width,
      height: canva.height
    }
  end

  def render("show.json", %{canva: canva}) do
    %{
      width: canva.width,
      height: canva.height,
      rectangles: render_many(canva.rectangles, __MODULE__, "rectangle.json", as: :rectangle)
    }
  end

  def render("rectangle.json", %{rectangle: rectangle}) do
    rectangle
    |> convert_values()
    |> draw_column()
  end

  defp draw_column(points) do
    for x <- points.first_x..points.last_x, y <- points.first_y..points.last_y do
      %{x: x, y: y, text: draw_letter(x, y, points)}
    end
  end

  defp draw_letter(x, _y, %{first_x: x, outline: outline}), do: outline
  defp draw_letter(x, _y, %{last_x: x, outline: outline}), do: outline

  defp draw_letter(_x, y, %{first_y: y, outline: outline}), do: outline
  defp draw_letter(_x, y, %{last_y: y, outline: outline}), do: outline

  defp draw_letter(_x, _y, %{fill: fill}), do: fill

  defp convert_values(rectangle) do
    %{
      width: rectangle.width,
      height: rectangle.height,
      first_x: rectangle.point_x,
      last_x: rectangle.point_x + (rectangle.width - 1),
      first_y: rectangle.point_y,
      last_y: rectangle.point_y + (rectangle.height - 1),
      fill: rectangle.fill,
      outline: rectangle.outline
    }
  end
end
