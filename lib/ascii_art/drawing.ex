defmodule AsciiArt.Drawing do
  @moduledoc """
  The Drawing context.
  """

  import Ecto.Query, warn: false
  alias AsciiArt.Repo

  alias AsciiArt.Drawing.{Canva, Rectangle}

  @doc """
  Return a list of all canvas

  This is just an auxiliary function to see the whole
  list of canvas on the frontend.
  Performance wasn't taken into consideration here.

  ## Examples

      iex> AsciiArt.Drawing.list_canvas()
      [%Canva{}, %Canva{}]

      iex> AsciiArt.Drawing.list_canvas()
      []

  """
  def list_canvas() do
    Repo.all(Canva)
  end

  @doc """
  Gets a single canva.

  Returns `{:error, :not_found}` if the Canva does not exist.

  ## Examples

      iex> AsciiArt.Drawing.get_canva("f865c650-b808-40cf-a6bf-64bc6e37e0d0")
      {:ok, %Canva{}}

      iex> AsciiArt.Drawing.get_canva("14c8ff9e-cb9f-41fa-8c15-43176953cfe7")
      {:error, :not_found}

  """
  def get_canva(id) do
    case Repo.get(Canva, id) do
      nil -> {:error, :not_found}
      canva -> {:ok, canva}
    end
  end

  @doc """
  Gets a single canva with the associations loaded.

  Returns `{:error, :not_found}` if the Canva does not exist.

  ## Examples

      iex> AsciiArt.Drawing.get_canva_with_associations("f865c650-b808-40cf-a6bf-64bc6e37e0d0")
      {:ok, %Canva{}}

      iex> AsciiArt.Drawing.get_canva_with_associations("14c8ff9e-cb9f-41fa-8c15-43176953cfe7")
      {:error, :not_found}

  """
  def get_canva_with_associations(id) do
    case Repo.get(Canva, id) |> Repo.preload([:rectangles]) do
      nil -> {:error, :not_found}
      canva -> {:ok, canva}
    end
  end

  @doc """
  Creates a canva.

  ## Examples

      iex> AsciiArt.Drawing.create_canva(%{})
      {:ok, %Canva{}}

      iex> AsciiArt.Drawing.create_canva(%{width: -1000, height: 100})
      {:error, %Ecto.Changeset{}}

  """
  def create_canva(attrs \\ %{width: 500, height: 500}) do
    %Canva{}
    |> Canva.changeset(attrs)
    |> Repo.insert()
  end

  @doc """
  Creates a rectangle on a canva.

  ## Examples
      iex> {:ok, canva} = AsciiArt.Drawing.create_canva(%{})

      AsciiArt.Drawing.draw_rectangle(canva, %{point_x: 0})
      {:ok, %Rectangle{}}

      iex>

    AsciiArt.Drawing.draw_rectangle(canva, %{point_x: -1000})
      {:error, %Ecto.Changeset{}}

  """

  def draw_rectangle(canva, attrs) do
    %Rectangle{canva_id: canva.id}
    |> Rectangle.changeset(attrs)
    |> Repo.insert()
  end
end
