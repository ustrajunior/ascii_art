defmodule AsciiArt.Drawing.Canva do
  @moduledoc """
  A Canvas is the primary part of the whole drawing.
  One could generate a new Canvas specifying the width and height values.
  The values should follow a few rules for the size. The min value is 1, and the maximum is 1000.

  The Canvas has a list of drawings that will form the image to be displayed.
  """

  use Ecto.Schema
  import Ecto.Changeset

  alias AsciiArt.Drawing.Rectangle

  @primary_key {:id, :binary_id, autogenerate: true}
  @foreign_key_type :binary_id
  schema "canvas" do
    field :height, :integer
    field :width, :integer

    has_many :rectangles, Rectangle

    timestamps()
  end

  @doc false
  def changeset(canva, attrs) do
    canva
    |> cast(attrs, [:width, :height])
    |> validate_required([:width, :height])
    |> validate_number(:width, greater_than: 0, less_than_or_equal_to: 1000)
    |> validate_number(:height, greater_than: 0, less_than_or_equal_to: 1000)
  end
end
