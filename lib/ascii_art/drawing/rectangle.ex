defmodule AsciiArt.Drawing.Rectangle do
  @moduledoc """
  A rectangle is a drawing that we can add to a canvas.
  Right now, this is the only available figure.

  The rectangle is a simple shape, and a canvas can hold as many as needed.
  """

  use Ecto.Schema
  import Ecto.Changeset

  alias AsciiArt.Drawing.Canva

  @primary_key {:id, :binary_id, autogenerate: true}
  @foreign_key_type :binary_id
  schema "rectangles" do
    field :point_x, :integer
    field :point_y, :integer
    field :width, :integer
    field :height, :integer
    field :fill, :string
    field :outline, :string

    belongs_to :canva, Canva

    timestamps()
  end

  @doc false
  def changeset(rectangle, attrs) do
    rectangle
    |> cast(attrs, [:canva_id, :point_x, :point_y, :width, :height, :fill, :outline])
    |> validate_required([:canva_id, :point_x, :point_y, :width, :height])
    |> validate_number(:width, greater_than: 0, less_than_or_equal_to: 1000)
    |> validate_number(:height, greater_than: 0, less_than_or_equal_to: 1000)
    |> validate_number(:point_x, greater_than_or_equal_to: 0, less_than_or_equal_to: 1000)
    |> validate_number(:point_y, greater_than_or_equal_to: 0, less_than_or_equal_to: 1000)
    |> validate_lines()
  end

  defp validate_lines(changeset) do
    if get_change(changeset, :fill) || get_change(changeset, :outline) do
      changeset
    else
      changeset
      |> add_error(:fill, "can't be blank")
      |> add_error(:outline, "can't be blank")
    end
  end
end
