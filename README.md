# Description

This repo is the backend for the AsciiArt project. You can find the frontend on this [link](https://gitlab.com/ustrajunior/ascii_art_client).

The AsciiArt returns data points to create images of rectangles formed by text provided when creating a new drawing on the canvas. You can see examples of the generated images on the [frontend project](https://gitlab.com/ustrajunior/ascii_art_client).

# How to run


You need to install the dependencies, create the database and migrate it, and then you can run the server. You can follow the steps below.

  * Install dependencies with `mix deps.get`
  * Prepare the database with `mix do ecto.create, ecto.migrate`
  * Create a couple of Canvas with `mix run priv/repo/seeds.exs`, so you have something to play with
  * Start Phoenix endpoint with `mix phx.server` or inside IEx with `iex -S mix phx.server`

If the backend running, you can follow the instructions on the [frontend project](https://gitlab.com/ustrajunior/ascii_art_client).

# Create a new canvas

To create a new canvas, you can use the console started inside IEx.

First create a new canvas:

```elixir
{:ok, canva} = Drawing.create_canva()
```

A canvas can have as many rectangles as you want. To create a new one, use:

```elixir
Drawing.draw_rectangle(canva, %{
  point_x: 3,
  point_y: 2,
  width: 5,
  height: 3,
  outline: "@",
  fill: "X"
})
```

# How to test

Tests use ExUnit so that you can run them with:

```
mix test
```
